const bootstrapper = document.implementation.createHTMLDocument("");
const closedShadowRootRegistry = new WeakMap();
const defineProperty = Object.defineProperty;
const forEach = Array.prototype.forEach;

function isElementConnected(element) {
  return "isConnected" in element
    ? element.isConnected
    : document.contains(element);
}
function unique(arr) {
  return [...new Set(arr)];
}
function diff(arr1, arr2) {
  return arr1.filter((value) => !arr2.includes(value));
}
function getShadowRoot(element) {
  return element.shadowRoot || closedShadowRootRegistry.get(element);
}

const cssStyleSheetMethods = [
  "addRule",
  "deleteRule",
  "insertRule",
  "removeRule",
];
const NonConstructedStyleSheet = CSSStyleSheet;
const nonConstructedProto = NonConstructedStyleSheet.prototype;
nonConstructedProto.replace = function () {
  return Promise.reject(
    new DOMException("Can't call replace on non-constructed CSSStyleSheets.")
  );
};
nonConstructedProto.replaceSync = function () {
  throw new DOMException(
    "Failed to execute 'replaceSync' on 'CSSStyleSheet': Can't call replaceSync on non-constructed CSSStyleSheets."
  );
};
function isCSSStyleSheetInstance(instance) {
  return typeof instance === "object"
    ? proto$1.isPrototypeOf(instance) ||
        nonConstructedProto.isPrototypeOf(instance)
    : false;
}
function isNonConstructedStyleSheetInstance(instance) {
  return typeof instance === "object"
    ? nonConstructedProto.isPrototypeOf(instance)
    : false;
}
const $basicStyleElement = new WeakMap();
const $locations = new WeakMap();
const $adoptersByLocation = new WeakMap();
const $appliedMethods = new WeakMap();
function addAdopterLocation(sheet, location) {
  const adopter = document.createElement("style");
  $adoptersByLocation.get(sheet).set(location, adopter);
  $locations.get(sheet).push(location);
  return adopter;
}
function getAdopterByLocation(sheet, location) {
  return $adoptersByLocation.get(sheet).get(location);
}
function removeAdopterLocation(sheet, location) {
  $adoptersByLocation.get(sheet).delete(location);
  $locations.set(
    sheet,
    $locations.get(sheet).filter((_location) => _location !== location)
  );
}
function restyleAdopter(sheet, adopter) {
  requestAnimationFrame(function () {
    adopter.textContent = $basicStyleElement.get(sheet).textContent;
    $appliedMethods.get(sheet).forEach(function (command) {
      return adopter.sheet[command.method].apply(adopter.sheet, command.args);
    });
  });
}
function checkInvocationCorrectness(self) {
  if (!$basicStyleElement.has(self)) {
    throw new TypeError("Illegal invocation");
  }
}
class ConstructedStyleSheet {
  constructor() {
    const style = document.createElement("style");
    bootstrapper.body.appendChild(style);
    $basicStyleElement.set(this, style);
    $locations.set(this, []);
    $adoptersByLocation.set(this, new WeakMap());
    $appliedMethods.set(this, []);
  }
  replace(contents) {
    try {
      this.replaceSync(contents);
      return Promise.resolve(this);
    } catch (e) {
      return Promise.reject(e);
    }
  }
  replaceSync(contents) {
    checkInvocationCorrectness(this);
    if (typeof contents === "string") {
      $basicStyleElement.get(this).textContent = contents;
      $appliedMethods.set(this, []);
      $locations.get(this).forEach((location) => {
        if (location.isConnected()) {
          restyleAdopter(this, getAdopterByLocation(this, location));
        }
      });
    }
  }
}
const proto$1 = ConstructedStyleSheet.prototype;
defineProperty(proto$1, "cssRules", {
  configurable: true,
  enumerable: true,
  get: function cssRules() {
    checkInvocationCorrectness(this);
    return $basicStyleElement.get(this).sheet.cssRules;
  },
});
defineProperty(proto$1, "media", {
  configurable: true,
  enumerable: true,
  get: function media() {
    checkInvocationCorrectness(this);
    return $basicStyleElement.get(this).sheet.media;
  },
});
cssStyleSheetMethods.forEach(function (method) {
  proto$1[method] = function (...args) {
    checkInvocationCorrectness(this);
    $appliedMethods.get(this).push({ method: method, args: args });
    $locations.get(this).forEach((location) => {
      if (location.isConnected()) {
        const sheet = getAdopterByLocation(this, location).sheet;
        sheet[method].apply(sheet, args);
      }
    });
    const basicSheet = $basicStyleElement.get(this).sheet;
    return basicSheet[method].apply(basicSheet, args);
  };
});
defineProperty(ConstructedStyleSheet, Symbol.hasInstance, {
  configurable: true,
  value: isCSSStyleSheetInstance,
});

const defaultObserverOptions = {
  childList: true,
  subtree: true,
};
const locations = new WeakMap();
function getAssociatedLocation(element) {
  let location = locations.get(element);
  if (!location) {
    location = new ElementLocation(element);
    locations.set(element, location);
  }
  return location;
}
function attachAdoptedStyleSheetProperty(constructor) {
  defineProperty(constructor.prototype, "adoptedStyleSheets", {
    configurable: true,
    enumerable: true,
    get() {
      return getAssociatedLocation(this).sheets;
    },
    set(sheets) {
      getAssociatedLocation(this).update(sheets);
    },
  });
}
function traverseWebComponents(node, callback) {
  const iter = document.createNodeIterator(
    node,
    NodeFilter.SHOW_ELEMENT,
    (foundNode) =>
      getShadowRoot(foundNode)
        ? NodeFilter.FILTER_ACCEPT
        : NodeFilter.FILTER_REJECT,
    null,
    false
  );
  for (let next = void 0; (next = iter.nextNode()); ) {
    callback(getShadowRoot(next));
  }
}
const $element = new WeakMap();
const $uniqueSheets = new WeakMap();
const $observer = new WeakMap();
function isExistingAdopter(self, element) {
  return (
    element instanceof HTMLStyleElement &&
    $uniqueSheets.get(self).some(function (sheet) {
      return getAdopterByLocation(sheet, self);
    })
  );
}
function getAdopterContainer(self) {
  const element = $element.get(self);
  return element instanceof Document ? element.body : element;
}
function adopt(self) {
  const styleList = document.createDocumentFragment();
  const sheets = $uniqueSheets.get(self);
  const observer = $observer.get(self);
  const container = getAdopterContainer(self);
  observer.disconnect();
  sheets.forEach((sheet) =>
    styleList.appendChild(
      getAdopterByLocation(sheet, self) || addAdopterLocation(sheet, self)
    )
  );
  container.insertBefore(styleList, null);
  observer.observe(container, defaultObserverOptions);
  sheets.forEach((sheet) =>
    restyleAdopter(sheet, getAdopterByLocation(sheet, self))
  );
}
class ElementLocation {
  sheets = [];

  constructor(element) {
    $element.set(this, element);
    $uniqueSheets.set(this, []);
    $observer.set(
      this,
      new MutationObserver((mutations, observer) => {
        if (!document) {
          observer.disconnect();
          return;
        }
        mutations.forEach((mutation) => {
          forEach.call(mutation.addedNodes, (node) => {
            if (!(node instanceof Element)) {
              return;
            }
            traverseWebComponents(node, (root) => {
              getAssociatedLocation(root).connect();
            });
          });
          forEach.call(mutation.removedNodes, (node) => {
            if (!(node instanceof Element)) {
              return;
            }
            if (isExistingAdopter(this, node)) {
              adopt(this);
            }
            traverseWebComponents(node, (root) =>
              getAssociatedLocation(root).disconnect()
            );
          });
        });
      })
    );
  }
  isConnected() {
    const element = $element.get(this);
    return element instanceof Document
      ? element.readyState !== "loading"
      : isElementConnected(element.host);
  }
  connect() {
    const container = getAdopterContainer(this);
    $observer.get(this).observe(container, defaultObserverOptions);
    if ($uniqueSheets.get(this).length > 0) {
      adopt(this);
    }
    traverseWebComponents(container, (root) =>
      getAssociatedLocation(root).connect()
    );
  }
  disconnect() {
    $observer.get(this).disconnect();
  }
  update(sheets) {
    const locationType =
      $element.get(this) === document ? "Document" : "ShadowRoot";
    if (!Array.isArray(sheets)) {
      throw new TypeError(
        "Failed to set the 'adoptedStyleSheets' property on " +
          locationType +
          ": Iterator getter is not callable."
      );
    }
    if (!sheets.every(isCSSStyleSheetInstance)) {
      throw new TypeError(
        "Failed to set the 'adoptedStyleSheets' property on " +
          locationType +
          ": Failed to convert value to 'CSSStyleSheet'"
      );
    }
    if (sheets.some(isNonConstructedStyleSheetInstance)) {
      throw new TypeError(
        "Failed to set the 'adoptedStyleSheets' property on " +
          locationType +
          ": Can't adopt non-constructed stylesheets"
      );
    }
    this.sheets = sheets;
    const oldUniqueSheets = $uniqueSheets.get(this);
    const uniqueSheets = unique(sheets);
    const removedSheets = diff(oldUniqueSheets, uniqueSheets);
    removedSheets.forEach((sheet) => {
      getAdopterByLocation(sheet, this).remove();
      removeAdopterLocation(sheet, this);
    });
    $uniqueSheets.set(this, uniqueSheets);
    if (this.isConnected() && uniqueSheets.length > 0) {
      adopt(this);
    }
  }
}

window.CSSStyleSheet = ConstructedStyleSheet;
attachAdoptedStyleSheetProperty(Document);
if ("ShadowRoot" in window) {
  attachAdoptedStyleSheetProperty(ShadowRoot);
  const proto = Element.prototype;
  const attach_1 = proto.attachShadow;
  proto.attachShadow = function attachShadow(init) {
    const root = attach_1.call(this, init);
    if (init.mode === "closed") {
      closedShadowRootRegistry.set(this, root);
    }
    return root;
  };
}

getAssociatedLocation(document).connect();
