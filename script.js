// deno-fmt-ignore-file
// deno-lint-ignore-file
// This code was bundled using `deno bundle` and it's not recommended to edit it manually

const cssstore = new WeakMap();
class ShadowHTMLElement extends HTMLElement {
    constructor(element){
        super();
        const shadow = this.attachShadow({
            mode: "open"
        });
        shadow.replaceChildren(element);
        const sheet = cssstore.get(this.constructor);
        if (sheet) this.shadowRoot.adoptedStyleSheets = [
            sheet
        ];
    }
    apply_css(sheet) {
        this.shadowRoot.adoptedStyleSheets = [
            ...this.shadowRoot.adoptedStyleSheets,
            sheet, 
        ];
    }
    shadow_element(id) {
        return this.shadowRoot.getElementById(id);
    }
}
function customElement(name) {
    return (clazz)=>{
        customElements.define(name, clazz);
    };
}
function css(strings, ...values) {
    const style = new CSSStyleSheet();
    style.replaceSync(String.raw(strings, ...values));
    return (clazz)=>{
        cssstore.set(clazz, style);
    };
}
function human_size(bytes) {
    if (bytes == 0) return "0.00 B";
    var e = Math.floor(Math.log(bytes) / Math.log(1024));
    return (bytes / Math.pow(1024, e)).toFixed(2) + " " + " KMGTP".charAt(e) + "B";
}
const timefmt = new Intl.DateTimeFormat([], {
    dateStyle: "short",
    timeStyle: "short"
});
function human_time(num) {
    return timefmt.format(num * 1000);
}
function toDash(name) {
    return name.replace(/([A-Z])/g, (g)=>`-${g[0].toLowerCase()}`
    ).replace(/^\$/g, "--");
}
function processChildren(target, children = []) {
    for (const child of children){
        if (child == null || typeof child == "boolean") continue;
        if (Array.isArray(child)) processChildren(target, child);
        else if (child instanceof Node) target.appendChild(child);
        else target.appendChild(document.createTextNode("" + child));
    }
}
const fragment = Symbol();
function createElement(name, attr, ...children) {
    if (name === fragment) {
        const ret = document.createDocumentFragment();
        processChildren(ret, children);
        return ret;
    } else if (typeof name == "function" && name.length == 1) {
        return new name({
            ...attr,
            children
        });
    }
    const ret = typeof name == "function" ? new name() : document.createElement(name);
    if (attr) for(const key in attr){
        const value = attr[key];
        if (typeof value != "string") {
            if (key == "data") Object.assign(ret.dataset, value);
            else if (key == "class") {
                if (Array.isArray(value)) for (const name of value)ret.classList.add(toDash(name));
                else for(const name1 in value)ret.classList.toggle(toDash(name1), !!value[name1]);
            } else if (key == "style") {
                for(const name in value)ret.style.setProperty(toDash(name), value[name]);
            } else if (key == "_") {
                Object.assign(ret, value);
            } else ret.setAttribute(key, value);
        } else if (key == "html") {
            ret.innerHTML = value;
        } else {
            ret.setAttribute(key, value);
        }
    }
    processChildren(ret, children);
    return ret;
}
const __default = Object.assign(createElement, {
    fragment
});
var _class, _class1, _class2, _class3, _class4, _class5;
function update_scroll() {
    const state = Math.min(100, window.scrollY) / 100;
    document.body.style.setProperty("--header-state", `${state}`);
}
window.addEventListener("scroll", update_scroll);
update_scroll();
const chat_id = document.querySelector(`meta[name="chat"]`).getAttribute("content");
var _dec = css`
  :host {
    display: flex;
    position: fixed;
    font-size: 30px;
    line-height: 30px;
    top: 0;
    width: 100%;
    max-width: var(--max-width);
    background-color: var(--bgcolor);
    z-index: 1;
  }
  slot {
    display: block;
    flex: 1;
    padding: calc(60px - var(--header-state) * 50px) 0;
  }
  ::slotted(h1) {
    margin: 0;
    text-align: center;
    font-size: inherit;
    font-weight: 100;
  }
  .horn {
    position: absolute;
    width: 20px;
    height: 20px;
    background-color: red;
    bottom: -20px;
    background-color: var(--bgcolor);
  }
  .horn.left {
    left: 10px;
    clip-path: path("M 0 0 L 0 20 A 20 20 180 0 1 20 0 Z");
  }
  .horn.right {
    right: 10px;
    clip-path: path("M 0 0 A 20 20 180 0 1 20 20 L 20 0 Z");
  }
`, _dec1 = customElement("tg-header");
let TgHeader = _class = _dec1(_class = _dec((_class = class TgHeader extends ShadowHTMLElement {
    constructor(){
        super(__default(__default.fragment, null, __default("slot", null), __default("div", {
            class: "horn left"
        }), __default("div", {
            class: "horn right"
        })));
    }
}) || _class) || _class) || _class;
var _dec2 = css`
  :host {
    display: flex;
    flex-direction: column;
    gap: 10px;
    padding: 10px;
    background-color: #eee;
    margin: 0 10px;
    overflow-wrap: break-word;
  }
  ::slotted(.messages) {
    display: contents;
  }
`, _dec3 = customElement("tg-content");
let TgContent = _class1 = _dec3(_class1 = _dec2((_class1 = class TgContent extends ShadowHTMLElement {
    constructor(){
        super(__default("slot", null));
    }
}) || _class1) || _class1) || _class1;
var _dec4 = css`
  :host {
    display: block;
    background-color: #fff;
    overflow-wrap: break-word;
    white-space: pre-wrap;
    border-radius: 15px;
    padding: 10px;
    padding-bottom: 20px;
    max-width: fit-content;
    min-width: 120px;
    margin-right: 35px;
    position: relative;
  }

  :host(:empty) {
    display: none;
  }

  #time {
    position: absolute;
    right: 5px;
    bottom: 5px;
    font-size: 50%;
  }

  #link {
    position: absolute;
    width: 25px;
    height: 25px;
    right: -35px;
    bottom: 0;
    background-color: white;
    border-radius: 20px;
    text-align: center;
    user-select: none;
    cursor: pointer;
  }
`, _dec5 = customElement("tg-message");
let TgMessage = _class2 = _dec5(_class2 = _dec4((_class2 = class TgMessage extends ShadowHTMLElement {
    constructor(){
        super(__default(__default.fragment, null, __default("slot", null), __default("div", {
            id: "time"
        }), __default("div", {
            id: "link"
        }, "🔗")));
        this.shadow_element("link").onclick = this.invoke.bind(this);
    }
    connectedCallback() {
        const time_el = this.shadow_element("time");
        time_el.textContent = human_time(+this.getAttribute("date"));
        const edit_date = this.getAttribute("edit_date");
        if (edit_date) {
            time_el.title = "edited, " + human_time(+edit_date);
        }
    }
    invoke() {
        open(`https://t.me/c/${chat_id}/${this.getAttribute("msgid")}`, "Telegram Link", "height=400,width=600");
    }
}) || _class2) || _class2) || _class2;
var _dec6 = css`
  #filesize:not(:empty)::before {
    content: "(";
  }
  #filesize:not(:empty)::after {
    content: ")";
  }
  :host {
    display: block;
  }
`, _dec7 = customElement("tg-message-document");
let TgMessageDocument = _class3 = _dec7(_class3 = _dec6((_class3 = class TgMessageDocument extends ShadowHTMLElement {
    constructor(){
        super(__default(__default.fragment, null, __default("slot", null), __default("span", {
            id: "filesize"
        }), __default("span", {
            id: "date"
        })));
    }
    connectedCallback() {
        this.shadow_element("filesize").textContent = human_size(+this.getAttribute("size"));
    }
}) || _class3) || _class3) || _class3;
var _dec8 = customElement("tg-message-entity");
let TgMessageEntity = _class4 = _dec8((_class4 = class TgMessageEntity extends ShadowHTMLElement {
    constructor(){
        super(__default("slot", null));
    }
}) || _class4) || _class4;
var _dec9 = css`
  :host {
    display: contents;
  }
  .horn {
    position: absolute;
    width: 20px;
    height: 20px;
    background-color: red;
    top: -20px;
    background-color: var(--bgcolor);
  }
  .horn.left {
    left: 10px;
    clip-path: path("M 0 0 A 20 20 180 0 0 20 20 L 0 20 Z");
  }
  .horn.right {
    right: 10px;
    clip-path: path("M 0 20 A 20 20 180 0 0 20 0 L 20 20 Z");
  }
  #bottom-bar {
    position: sticky;
    bottom: 0;
    left: 0;
    right: 0;
    height: 10px;
    background-color: var(--bgcolor);
  }
`, _dec10 = customElement("tg-footer");
let TgFooter = _class5 = _dec10(_class5 = _dec9((_class5 = class TgFooter extends ShadowHTMLElement {
    constructor(){
        super(__default(__default.fragment, null, __default("div", {
            id: "bottom-bar"
        }, __default("div", {
            class: "horn left"
        }), __default("div", {
            class: "horn right"
        })), __default("div", null, "CONTENT")));
    }
}) || _class5) || _class5) || _class5;
export { TgHeader as TgHeader };
export { TgContent as TgContent };
export { TgMessage as TgMessage };
export { TgMessageDocument as TgMessageDocument };
export { TgMessageEntity as TgMessageEntity };
export { TgFooter as TgFooter };
